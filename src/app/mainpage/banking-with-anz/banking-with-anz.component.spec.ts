import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BankingWithAnzComponent } from './banking-with-anz.component';

describe('BankingWithAnzComponent', () => {
  let component: BankingWithAnzComponent;
  let fixture: ComponentFixture<BankingWithAnzComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BankingWithAnzComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BankingWithAnzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
