import { Component, OnInit } from '@angular/core';
import { TypeaheadService } from 'src/app/services/qms/typeahead.service';
import { HighlightingService } from 'src/app/services/content/highlighting.service';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { ConversationtriggerService } from 'src/app/services/content/conversationtrigger.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { landingpageSearchTermAction } from 'src/app/state/landingpage.action';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  searchterm:string = "";
  phrases: Array<string> = [];
  conversationtriggers:Array<string> = [];
  show_autopopulate_div:boolean = false;
  show_autopopulate_conversation_div:boolean = false;

  stopPreviousRequest$ = new Subject<void>();

  constructor(private svc:TypeaheadService, 
              private highlightingsvc:HighlightingService, 
              private conversationtriggersvc:ConversationtriggerService,
              private router: Router,
              private store:Store<any>) { 

                this.router.routeReuseStrategy.shouldReuseRoute = () => false;
              }
  
  ngOnInit(): void {

  }

  onEnter(){
    this.show_autopopulate_conversation_div = false;
    this.show_autopopulate_div = false;
    this.store.dispatch(landingpageSearchTermAction({
      SearchTerm: this.searchterm,
      IsConversation: false
    }));
    this.router.navigate(['/results'], {queryParams: {keyword: this.searchterm}});
  }
  TermExpansionClicked(term:string){
    this.searchterm = term.replace(/<[^>]*>/g, '');;
    this.callTypeAhead(this.searchterm);
    this.show_autopopulate_conversation_div = false;
    this.show_autopopulate_div = false;
    this.store.dispatch(landingpageSearchTermAction({
      SearchTerm: this.searchterm,
      IsConversation: false
    }));
    this.router.navigate(['/results'], {queryParams: {keyword: this.searchterm}});

  }
  ConversationTriggerClicked(trigger:string){
    let clean_trigger = trigger.replace(/<[^>]*>/g, "");
    this.searchterm = clean_trigger;
    this.store.dispatch(landingpageSearchTermAction({
      SearchTerm: this.searchterm,
      IsConversation: true
    }));
    this.router.navigate(['/results'], {queryParams: {keyword: clean_trigger}});
  }
  capitalize(term:string): string{
    let ret_value:string  = term.trim();
    ret_value = ret_value.charAt(0).toUpperCase() + ret_value.slice(1);
    return ret_value;
  }
  callTypeAhead(searchterm:string){
    this.searchterm = searchterm;
    this.stopPreviousRequest$.next();

    this.svc.get_term_expansion_response(searchterm).pipe(takeUntil(this.stopPreviousRequest$)).subscribe(response => {
      if ( response.autnresponse.responsedata.expansion !==undefined){
            this.phrases = response.autnresponse.responsedata.expansion.map(expansion => expansion.$);
            this.show_autopopulate_div = true;

              // response.autnresponse.responsedata.expansion.forEach(ex => {
              // this.highlightingsvc.get_term_highlighting(query, ex.$).subscribe(highlightresponse => {
              //   let highlight = highlightresponse.autnresponse.responsedata.hit.content;
              //   this.phrases.push(highlight);
              //   this.show_autopopulate_div = true;
              // });

            // });
          }
          else{
            this.show_autopopulate_div = false;
          }
    });

    this.conversationtriggersvc.get_conversation_triggers(searchterm).pipe(takeUntil(this.stopPreviousRequest$)).subscribe(response => {
      var numhits_int = +response.autnresponse.responsedata.numhits;
      if(numhits_int > 0){
        this.conversationtriggers = response.autnresponse.responsedata.hit.map(hit => hit.content.DOCUMENT.CONFIRMATION);
        this.show_autopopulate_conversation_div = true;
      }
      else{
        this.show_autopopulate_conversation_div = false;
      }
    })
  }

}
