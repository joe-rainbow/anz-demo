import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { HighlightPipe } from './services/content/highlight.pipe';
import { MainpageComponent } from './mainpage/mainpage.component';
import { CovidComponent } from './mainpage/covid/covid.component';
import { BankingWithAnzComponent } from './mainpage/banking-with-anz/banking-with-anz.component';
import { SearchresultsComponent } from './searchresults/searchresults.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { StoreModule } from '@ngrx/store';
import { CapitalizePipe, StripHTMLPipe } from './services/content/capitalize.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabsetModule } from '@ux-aspects/ux-aspects';
import { landingpageReducer } from './state/landingpage.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HighlightPipe,
    StripHTMLPipe,
    CapitalizePipe,
    MainpageComponent,
    CovidComponent,
    BankingWithAnzComponent,
    SearchresultsComponent,
    LandingpageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    BrowserModule,
    TabsetModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({landingpage:landingpageReducer}),
    StoreDevtoolsModule.instrument({ name: 'ANZ - Demo IDOL DevTools', maxAge: 25, logOnly: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
