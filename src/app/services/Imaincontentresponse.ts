export interface IMainContentResponse {
    autnresponse: Autnresponse;
}

export interface Autnresponse {
    action:       string;
    response:     string;
    responsedata: Responsedata;
}

export interface Responsedata {
    hit:          Hit[];
    numhits:      string;
    totalhits:    string;
    totaldbdocs:  string;
    querysummary: string;
    qs:           Qs;
    qmsstate:     string;
}

export interface Hit {
    reference: string;
    id:        string;
    section:   string;
    weight:    string;
    links:     string;
    database:  Database;
    title:     string;
    summary:   string;
    content:   Content;
}

export interface Content {
    DOCUMENT: Document;
}

export interface Document {
    DRETITLE: string;
}

export enum Database {
    Anz = "ANZ",
}

export interface Qs {
    element: Element[];
}

export interface Element {
    "@pdocs":   string;
    "@poccs":   string;
    "@cluster": string;
    "@docs":    string;
    "@ids":     string;
    $:          string;
}
