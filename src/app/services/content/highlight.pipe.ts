import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HighlightingService } from 'src/app/services/content/highlighting.service';
import { HighlightResponse } from 'src/app/services/content/Ihighlight.response';

@Pipe({
    name: 'highlight'
})
export class HighlightPipe implements PipeTransform {

    transform(value: string, query: string): string {
        const regex = new RegExp(query, 'i');
        let unmatched = value.toLowerCase().replace(query.toLowerCase(),"");
        let return_value = value.toLowerCase().replace(unmatched, "<strong>" + unmatched + "</strong>");
        return return_value;
    }
}