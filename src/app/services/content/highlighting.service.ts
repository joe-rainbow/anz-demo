import { Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HighlightResponse } from './Ihighlight.response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HighlightingService {

  constructor(private http: HttpClient) 
  {

  }

  get_term_highlighting(term:string, sentence:string): Observable<HighlightResponse> {
    return this.http.get<HighlightResponse>(`http://${environment.hostname}:9100`, 
    {
      params: {
        'Action':'HIGHLIGHT',
        'Text': sentence,
        'Links': "DREFUZZY(" + term + ")",
        'Boolean': 'True',
        'StartTag':'<strong>',
        'responseFormat': 'simplejson'
      }
    });
  }
}
