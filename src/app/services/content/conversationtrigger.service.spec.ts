import { TestBed } from '@angular/core/testing';

import { ConversationtriggerService } from './conversationtrigger.service';

describe('ConversationtriggerService', () => {
  let service: ConversationtriggerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConversationtriggerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
