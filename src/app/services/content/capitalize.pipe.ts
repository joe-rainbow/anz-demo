import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'cap'
})
export class CapitalizePipe implements PipeTransform {

    transform(value: string): string {
        let retval = value.charAt(0).toUpperCase() + value.slice(1);
        return retval;
    }
}

@Pipe({
    name: 'striphtml'
})
export class StripHTMLPipe implements PipeTransform {
    transform(value:string): string {
        let retval = value.replace('<strong>','');
        retval = retval.replace('</strong>','');
        return retval;
    }
}