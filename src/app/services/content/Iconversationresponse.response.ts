
    export interface DOCUMENT {
        DREREFERENCE: string;
        DREDATE: string;
        DRETITLE: string;
        DREDBNAME: string;
        CONFIRMATION: string;
        DRECONTENT: string;
    }

    export interface Content {
        DOCUMENT: DOCUMENT;
    }

    export interface Hit {
        reference: string;
        id: string;
        section: string;
        weight: string;
        links: string;
        database: string;
        title: string;
        content: Content;
        highlighted:string;
    }

    export interface Responsedata {
        hit: Hit[];
        numhits: string;
        predicted: string;
        totalhits: string;
        totaldbdocs: string;
        totaldbsecs: string;
        qmsstate: string;
    }

    export interface Autnresponse {
        action: string;
        response: string;
        responsedata: Responsedata;
    }

    export interface ConversationResponse {
        autnresponse: Autnresponse;
    }

