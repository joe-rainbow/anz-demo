import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HighlightResponse } from './Ihighlight.response';
import { environment } from 'src/environments/environment';
import { ConversationResponse } from './Iconversationresponse.response';


@Injectable({
  providedIn: 'root'
})
export class ConversationtriggerService {

  constructor(private http: HttpClient) 
  {

  }

  get_conversation_triggers(searchterm:string): Observable<ConversationResponse> {
    return this.http.get<ConversationResponse>(`http://${environment.hostname}:16100`,{
      params:{
        'Action':'Query',
        'print':'fields',
        'printfields':'CONFIRMATION',
        'text': searchterm,
        'highlight':'Terms',
        'starttag':'<strong>',
        'endtag':'</strong>',
        'AgentBooleanField':'BOOLEANRESTRICTION',
        'responseformat':'simplejson'
      }
    });
  }
}

