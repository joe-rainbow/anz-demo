import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IMainContentResponse } from './Imaincontentresponse';

@Injectable({
  providedIn: 'root'
})
export class MaincontentService {

  constructor(private http: HttpClient) { }

  get_search_results(searchTerm: string, start:string) : Observable<IMainContentResponse>{
    return this.http.get<IMainContentResponse>(`http://${environment.hostname}:16000`, {
      params:{
        'Action':'Query',
        'text':searchTerm,
        'databasematch':'ANZ',
        'print':'fields',
        'printfields':'DRETITLE',
        'QuerySummary':'true',
        'maxresults':'10',
        'QuerySummaryAdvanced':'true',
        'Stemming':'True',
        'Summary':'Quick',
        'Characters':'420',
        'TotalResults':'true',
        'highlight':'SummaryTerms',
        'starttag':'<strong>',
        'endtag':'</strong>',
        'cluster':'true',
        'start':start,
        'ExpandQuery':'true',
        'ClusterThreshold':'70',
        'DefaultOperator':'WNEAR5',
        'responseformat':'simplejson'
      }
    });
  }
}
