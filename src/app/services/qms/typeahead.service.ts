import { Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { TypeAheadResponse } from './Itypeahead.response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TypeaheadService {

  constructor(private http: HttpClient) 
  {

  }

  get_term_expansion_response(term: string): Observable<TypeAheadResponse>{
    return this.http.get<TypeAheadResponse>(`http://${environment.hostname}:16000`,
    {
      params:{
        'Action':'TypeAhead',
        'Mode':'Index',
        'Text':term,
        'Behaviour':'Phrase',
        'MaxResults':'5',
        'responseFormat':'simplejson'
      }
    });
  }
}
