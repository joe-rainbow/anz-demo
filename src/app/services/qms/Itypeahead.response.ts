
export interface Expansion {
    '@score': string;
    '$': string;
}

export interface Responsedata {
    expansion: Expansion[];
}

export interface Autnresponse {
    action: string;
    response: string;
    responsedata: Responsedata;
}

export interface TypeAheadResponse {
    autnresponse: Autnresponse;
}
