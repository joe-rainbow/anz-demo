export interface Results{
    title: string,
    summary: string,
    link: string
}