import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { IMainContentResponse } from '../services/Imaincontentresponse';
import { MaincontentService } from '../services/maincontent.service';
import { Results } from './IResults';

@Component({
  selector: 'app-searchresults',
  templateUrl: './searchresults.component.html',
  styleUrls: ['./searchresults.component.css']
})
export class SearchresultsComponent implements OnInit {

  constructor(private route:ActivatedRoute, 
              private svc:MaincontentService,
              private store:Store<any>) { 
    this.route.queryParams.subscribe(params => {
      store.pipe(select('landingpage')).subscribe(v => {
        this.searchTerm = v.keyword;
        this.showconversationtab = v.showconversation;
      })
      this.get_search_results();
    })
  }
  start:string ='1';
  showresults: boolean = false;
  showconversationtab:boolean = false;
  totalresults:string = '';
  totaldbdocs:string = '';
  searchcoverage:string = '';
  searchresults: Results[] = [];
  querysummary:string = '';
  numhits:string = '';
  searchTerm: string = '';
  ngOnInit(): void {
  }
  get_next_results(){
    var start_int: number = + this.start;
    start_int = start_int + 10;
    this.start = start_int.toString();
  }
  get_search_results(){
    this.svc.get_search_results(this.searchTerm, this.start).subscribe(response => {
        this.searchresults = [];
        var numhits: number = +response.autnresponse.responsedata.numhits;
        var responsedata = response.autnresponse.responsedata;
        if (numhits > 0){
          this.numhits = responsedata.numhits;
          this.showresults = true;
          this.totalresults = responsedata.totalhits;
          this.totaldbdocs = responsedata.totaldbdocs;
          let coverage:number = +this.totalresults / +this.totaldbdocs;
          this.searchcoverage = coverage.toString();
          this.querysummary = responsedata.querysummary;
          responsedata.hit.map(result => {
            this.searchresults.push({
              title: result.title.substring(0, result.title.length - 5),
              summary: result.summary,
              link: result.reference
            })
          });

        }
        else{
          this.numhits = '0';
          this.showresults = false;
        }

    })
  }
}
