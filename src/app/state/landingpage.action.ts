import { createAction, props } from '@ngrx/store';

export enum SearchBoxActions {
    SearchTextAction = "[Landing Page] Search Text"
}

export const landingpageSearchTermAction = createAction(
    SearchBoxActions.SearchTextAction,
    props<{ SearchTerm: string, IsConversation: Boolean}>()
);
