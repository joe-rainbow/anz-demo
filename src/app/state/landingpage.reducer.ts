import { SearchBoxActions } from './landingpage.action';


export interface SearchState {
    keyword: string,
    showconversation:boolean
}

const initialState: SearchState = {
    keyword:'',
    showconversation: false
}

export function landingpageReducer(state: SearchState = initialState, action: any){
    switch(action.type){
        case SearchBoxActions.SearchTextAction:
            console.table(action);
            return {
                ...state,
                keyword: action.SearchTerm,
                showconversation: action.IsConversation
            }
        default:
            return state;
    }
}