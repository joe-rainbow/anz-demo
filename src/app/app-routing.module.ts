import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { SearchresultsComponent } from './searchresults/searchresults.component';

const routes: Routes = [
  {
    path: 'home', component: LandingpageComponent
  },
  {
    path: 'results', component: SearchresultsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
